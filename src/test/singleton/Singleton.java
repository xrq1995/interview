package test.singleton;

/**
 * 双重校验锁DCL 使用volatile的场景之一  实现单例模式
 */
public class Singleton {
    //注意需要用volatile关键字
    private static volatile Singleton instance = null;
    //构造函数私有化
    private Singleton(){}
    //双重检查加锁，只有在第一次实例化时，才启用同步机制，提高了性能。
    public static Singleton getInstance(){
        if (instance==null){
            synchronized(Singleton.class){
                if (instance==null){
                    instance = new Singleton(); //非原子操作
                }
            }
        }
        return instance;
    }
}
