package test.singleton;


/**
 * 懒汉 单例模式
 */
public class SafeLazyInitialization {
    private static SafeLazyInitialization newInstance =null;
    //私有化构造函数
    private SafeLazyInitialization(){}
    public static SafeLazyInitialization getNewInstance(){
        if (newInstance==null){
            newInstance = new SafeLazyInitialization();

        }
        return newInstance;
    }
}
