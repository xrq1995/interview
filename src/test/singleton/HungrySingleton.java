package test.singleton;

/**
 * 饿汉 单例模式
 */
public class HungrySingleton {
    private static final HungrySingleton newInstance = new HungrySingleton();
    private HungrySingleton(){}
    public static HungrySingleton getNewInstance(){
        return newInstance;
    }
}
